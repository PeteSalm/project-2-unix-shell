#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(int argc, char* argv[]) {
	
	// Muuttujien alustus
	int status;
	char command[100] = "test";
	char tempc[100] = "test";
	char *path = "/bin";
	char *buffer;
	size_t bufsize = 100;
	buffer = (char *)malloc(bufsize * sizeof(char));
	if( buffer == NULL) {
		perror("Unable to allocate buffer");
	exit(1);
	}
	FILE *fp;
	
	// Avaa tiedoston jos sellainen on annettu
	if (argc == 2) {
		fp = fopen(argv[1], "r");
		if (fp == NULL) {
			printf("wish: cannot open file\n");
			exit(1);
		}
	}

	while(strcmp(strtok(command," "),"exit")) {
		if (argc == 1) {			// Jos tiedostoa ei ole annettu, commandi promptin kautta
			printf("wish> ");
			getline(&buffer, &bufsize, stdin);	// Lukee std inputin ja poistaa newlinen
			strcpy(command, strtok(buffer,"\n"));
			
		} else if (argc == 2) {			// Jos tiedosto on annettu, commandi tiedostoa lukemalla
			fgets(command, 1000, fp);
			strtok(command,"\n");
		}
		strcpy(tempc, command);			// tempcommandi koska strtok rikkoo alkuperäisen 

		if (!strcmp(strtok(tempc," "),"cd")) {
			char *found;
			char *str;
			char *dir;
			str = strdup(command);
			int counter = 0;
			while((found = strsep(&str," ")) != NULL ) {
				counter++;
				if (counter == 2) {
					dir = strdup(found);	// Ottaa välilyönnin jälkeisen argumentin 
				}				// esim jos "cd /home" ottaa home
			}
			if (counter > 2) {
				printf("wish: too many arguments\n");	// Liikaa argumentteja
			} else if (counter == 1) {
				printf("wish: no directory specified\n");	// Liian vähän argumentteja
			} else {
				chdir(dir);		// Vaihtaa current directorya
				char s[100];
				if (strcmp(dir, getcwd(s, 100)))  	// Testaa vaihtuiko, jos ei niin virhe
					printf("wish: directory %s not found\n", dir);
			}
			

		} else if (!strcmp(strtok(tempc," "),"path")) {	
			char *found;
			char *str;
			char final[100] = "";
			str = strdup(command);
			int counter = 0;
			while((found = strsep(&str," ")) != NULL ) {	// Lisää kaikki "path" käskyn jälkeiset argumentit
				counter++;				// stringiin
				if (counter > 1) {
					strcat(final, found);
					strcat(final, " ");
				}
			}
			path = strdup(final);	// Laittaa valmiin stringin uudeksi pathiksi

		} else if (!strcmp(strtok(tempc," "),"exit")) {		// Exit jos kirjoittaa exit		
			exit(0);
		} else {
			char *bin;				// Muiden käskyjen kohdalla käyttää valmiita funktioita
			char *temppath = strdup(path);
			while((bin = strsep(&temppath," ")) != NULL) {	// Käy pathit läpi 
				char * const dir = strdup(bin);		
				char * const args[] = {bin, NULL};	// Luo argumentit execv:lle
								
				if (fork() == 0 ) {			// Suorittaa funktion jos mahdollista
					execv(dir, args);
					exit(1);
				} else {
					wait(&status);
				}
				
                       	}
		}
	
	}
	if (argc == 2)			// Jos tiedosto oli avattu, tiedosto suljetaan
		fclose(fp);

	return(0);
}
